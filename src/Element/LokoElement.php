<?php

namespace Drupal\commerce_loko\Element;

use Drupal\commerce_loko\Plugin\Field\FieldType\LokoItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a Loko parcel select form element.
 *
 * Usage example:
 * @code
 * $form['loko_locker'] = [
 *   '#type' => 'loko_locker',
 *   '#default_value' => [
 *     'parcel_locker_id' => 'a9522a9d-eaf5-11e7-ba66-005056b2fc3d',
 *     'parcel_locker_name' => 'Test parcel locker',
 *   ],
 * ];
 * @endcode
 *
 * @FormElement("loko_locker")
 */
class LokoElement extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo(): array {
    return [
      '#input' => TRUE,
      '#multiple' => FALSE,
      '#default_value' => NULL,
      '#process' => [
        [get_class($this), 'processElement'],
      ],
      '#element_validate' => [
        [get_class($this), 'validateLockerName'],
      ],
      '#theme_wrappers' => ['container'],
    ];
  }

  /**
   * Ensures all keys are set on the provided value.
   *
   * @param array $value
   *   The value.
   *
   * @return array
   *   The modified value.
   */
  public static function applyDefaults(array $value): array {
    $properties = [
      'parcel_locker_id',
      'parcel_locker_name',
    ];
    foreach ($properties as $property) {
      if (!isset($value[$property])) {
        $value[$property] = NULL;
      }
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    // Ensure both the default value and the input have all keys set.
    if (is_array($input)) {
      $input = static::applyDefaults($input);
    }

    $element['#default_value'] = (array) $element['#default_value'];
    $element['#default_value'] = static::applyDefaults($element['#default_value']);

    return is_array($input) ? $input : $element['#default_value'];
  }

  /**
   * Processes LokoElement form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @throws \Exception
   */
  public static function processElement(array &$element, FormStateInterface $form_state, array &$complete_form): array {
    $options = [];

    if ($element['#shipment_method'] === LokoItem::NOT_CONFIGURED_SHIPMENT) {
      $element['parcel_locker_id'] = [
        '#type' => 'markup',
        '#markup' => t('Loko shipment methods is not configured. Please contact the site administrator.'),
      ];
      return $element;
    }

    /** @var \Drupal\commerce_loko\LokoClientInterface $loko_client */
    $loko_client = \Drupal::service('commerce_loko.client');
    $loko_client->setShipmentByEntityId($element['#shipment_method']);
    $lockers = $loko_client->getAllParcelLockers();
    foreach ($lockers as $locker) {
      $options[$locker['id']] = $locker['name'];
    }

    $element['parcel_locker_id'] = [
      '#type' => 'select',
      '#title' => t('Select your parcel locker'),
      '#options' => $options,
      '#default_value' => $element['#value']['parcel_locker_id'] ?? NULL,
      '#required' => $element['#required'],
    ];
    return $element;
  }

  /**
   * Validate parcel locker ID to save parcel locker name.
   */
  public static function validateLockerName(array &$element, FormStateInterface $form_state, array &$complete_form) {
    $locker_id = $form_state->getValue($element['#parents'])['parcel_locker_id'];
    $locker_name = $element['parcel_locker_id']['#options'][$locker_id];
    $form_state->setValue(array_merge($element['#parents'], ['parcel_locker_name']), $locker_name);
  }

}
