<?php

namespace Drupal\commerce_loko;

use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodInterface as PluginEntityShippingMethodInterface;

/**
 * Provides an interface for Loko Client Service.
 */
interface LokoClientInterface {

  /**
   * Current company endpoint.
   */
  const API_PATH_CURRENT_COMPANY = '/api/Company/GetCurrentCompany';

  /**
   * Parcel lockers endpoint.
   */
  const API_PATH_ALL_PARCEL_LOCKERS = '/api/ParcelLocker/AllParcelLockers';

  /**
   * Set the shipping method plugin.
   *
   * @param \Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodInterface $shipping_method_plugin
   *   The shipping method plugin.
   */
  public function setShipmentByPlugin(PluginEntityShippingMethodInterface $shipping_method_plugin): void;

  /**
   * Set the shipping method plugin by id.
   *
   * @param string $id
   *   The ID of the shipping method plugin.
   */
  public function setShipmentByEntityId(string $id): void;

  /**
   * Validate the current API key.
   *
   * @return bool
   *   Return the access to the Client.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function validateToken(): bool;

  /**
   * Method for gets the all parcel lockers objects.
   *
   * @return array|false
   *   Gets the all parcel lockers.
   */
  public function getAllParcelLockers(): ?array;

  // @codingStandardsIgnoreStart
  /**
   * Method for gets the details for current parcel locker.
   *
   * @param string $parcel_locker_id
   *   The store identifier.
   *
   * @return array|false
   *   Array with data about current parcel locker.
   */
  // public function getDetailsParcelLocker(string $parcel_locker_id): ?array;

  /**
   * Method for gets the all free boxes from parcel locker by its id.
   *
   * @param string $parcel_locker_id
   *   The parcel locker identifier.
   *
   * @return bool
   *   TRUE is existed.
   */
  // public function checkIfParcelLockerExist(string $parcel_locker_id): bool;

  /**
   * Method for creating the reservation parcel.
   *
   * @param string $options
   *   The model.
   *
   * @return array|false
   *   Array of free Parcel Lockers.
   */
  // public function createReservation(string $options): ?array;

  /**
   * Method for creating a new parcel.
   *
   * @param string $options
   *   The model.
   *
   * @return array|false
   *   Array of free Parcel Lockers.
   */
   //  public function createParcel(string $options): ?array;

  /**
   * Method for confirming the reservation.
   *
   * @param string $parcel_id
   *   The store identifier.
   *
   * @return bool
   *   TRUE if reservation is confirmed.
   */
  // public function confirmReservation(string $parcel_id): bool;
  // @codingStandardsIgnoreEnd

}
