<?php

namespace Drupal\commerce_loko\Plugin\Field\FieldWidget;

use Drupal\commerce_loko\LokoClientInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the Loko widget.
 *
 * @FieldWidget(
 *   id = "loko_locker_widget",
 *   module = "commerce_loko",
 *   label = @Translation("Loko Parcel"),
 *   field_types = {
 *     "loko_locker"
 *   }
 * )
 */
class LokoWidget extends WidgetBase {

  /**
   * Loko client definition.
   *
   * @var \Drupal\commerce_loko\LokoClientInterface
   */
  protected LokoClientInterface $lokoClient;

  /**
   * Constructs a LokoWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\commerce_loko\LokoClientInterface $loko_client
   *   The Loko client.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, LokoClientInterface $loko_client) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->lokoClient = $loko_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('commerce_loko.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {

    /** @var \Drupal\commerce_loko\Plugin\Field\FieldType\LokoItem[] $items */
    if ($items[$delta]->isEmpty()) {
      $items[$delta]->parcel_locker_name = NULL;
      $items[$delta]->parcel_locker_id = NULL;
    }

    return [
      '#type' => 'loko_locker',
      '#shipment_method' => $this->getFieldSettings()['shipping_method'],
      '#default_value' => $items[$delta]->getValue(),
    ] + $element;
  }

}
