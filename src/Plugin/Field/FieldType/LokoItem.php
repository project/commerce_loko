<?php

namespace Drupal\commerce_loko\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the Loko field type.
 *
 * @FieldType(
 *   id = "loko_locker",
 *   label = @Translation("Loko Parcel Locker"),
 *   category = @Translation("Address"),
 *   description = @Translation("Field containing Loko parcel locker."),
 *   default_widget = "loko_locker_widget",
 *   default_formatter = "loko_locker_formatter"
 * )
 */
class LokoItem extends FieldItemBase {

  /**
   * A placeholder for not configured shipment methods.
   */
  public const NOT_CONFIGURED_SHIPMENT = 'no_shipment_method_configured';

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    return [
      'shipping_method' => LokoItem::NOT_CONFIGURED_SHIPMENT,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName(): ?string {
    return 'parcel_locker_id';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {

    $properties['parcel_locker_id'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Parcel locker ID'))
      ->setRequired(TRUE);

    $properties['parcel_locker_name'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Parcel locker'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'parcel_locker_id' => [
          'description' => 'Parcel locker ID',
          'type' => 'varchar',
          'length' => 36,
        ],
        'parcel_locker_name' => [
          'description' => 'Parcel locker name',
          'type' => 'varchar',
          'length' => 128,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    return $this->get('parcel_locker_id')->getValue() === NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {

    // @todo Implement ContainerInjectionInterface.
    $shipment_type_storage = \Drupal::entityTypeManager()->getStorage('commerce_shipping_method');
    $shipments = $shipment_type_storage->loadByProperties(['plugin' => 'loko_shipment']);

    $options = [];
    foreach ($shipments as $shipment) {
      /** @var \Drupal\commerce_shipping\Entity\ShippingMethod $shipment */
      $options[$shipment->id()] = $shipment->getName();
    }

    if (!$options) {
      $options[LokoItem::NOT_CONFIGURED_SHIPMENT] = $this->t('Please create Loko shipment method for this field usage.');
    }

    $element['shipping_method'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#options' => $options,
      '#title' => $this->t('Select Loko shipment method'),
      '#description' => $this->t('The shipment method to be used to get a configured API token.'),
      '#default_value' => $this->getSetting('shipping_method'),
      '#element_validate' => [[static::class, 'validateShipmentMethod']],
    ];

    return $element;
  }

  /**
   * Element validate callback for shipment method selection.
   *
   * @param array $element
   *   An associative array containing the properties and children of the
   *   generic form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form for the form this element belongs to.
   *
   * @see \Drupal\Core\Render\Element\FormElement::processPattern()
   */
  public static function validateShipmentMethod(array $element, FormStateInterface $form_state): void {
    if ($form_state->getUserInput()['settings']['shipping_method'] === LokoItem::NOT_CONFIGURED_SHIPMENT) {
      $form_state->setError($element, new TranslatableMarkup('Please create Loko shipment method for this field usage.'));
    }
  }

}
