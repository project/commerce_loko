<?php

namespace Drupal\commerce_loko\Plugin\Field\FieldFormatter;

use Drupal\commerce_loko\LokoClientInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the Loko formatter.
 *
 * @FieldFormatter(
 *   id = "loko_locker_formatter",
 *   label = @Translation("Loko Parcel Locker"),
 *   field_types = {
 *     "loko_locker"
 *   }
 * )
 */
class LokoFormatter extends FormatterBase {

  /**
   * Loko client definition.
   *
   * @var \Drupal\commerce_loko\LokoClientInterface
   */
  protected LokoClientInterface $lokoClient;

  /**
   * Constructs a LokoFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\commerce_loko\LokoClientInterface $loko_client
   *   The Loko API client.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LokoClientInterface $loko_client) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->lokoClient = $loko_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('commerce_loko.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta]['parcel_locker_name'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $item->parcel_locker_name,
        '#attributes' => [
          'class' => ['parcel-locker-id'],
          'data-locker-id' => $item->parcel_locker_id,
        ],
      ];
    }
    return $elements;
  }

}
