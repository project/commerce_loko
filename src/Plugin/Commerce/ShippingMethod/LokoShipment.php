<?php

namespace Drupal\commerce_loko\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_loko\LokoClient;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\commerce_shipping\ShippingService;
use Drupal\Core\Form\FormStateInterface;
use Drupal\state_machine\WorkflowManagerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the LokoShipment shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "loko_shipment",
 *   label = @Translation("Loko Shipment"),
 * )
 */
class LokoShipment extends ShippingMethodBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('commerce_loko.client')
    );
  }

  /**
   * Constructs a new LokoShipment object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\commerce_loko\LokoClient $lokoClient
   *   The loko client.
   */
  public function __construct(
    array $configuration,
          $plugin_id,
          $plugin_definition,
    PackageTypeManagerInterface $package_type_manager,
    WorkflowManagerInterface $workflow_manager,
    LokoClient $lokoClient
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);

    $this->services['default'] = new ShippingService('default', $this->t('Loko delivery'));
    $this->lokoClient = $lokoClient;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'rate_label' => '',
      'rate_description' => '',
      'rate_amount' => NULL,
      'services' => ['default'],
      'domain' => 'https://api.mint-dispatch.com',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);

    $amount = $this->configuration['rate_amount'];
    // A bug in the plugin_select form element causes $amount to be incomplete.
    if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
      $amount = NULL;
    }

    $form['rates_group'] = [
      '#type' => 'details',
      '#title' => $this->t('Rates settings'),
    ];
    $form['rates_group']['rate_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rate label'),
      '#description' => $this->t('Shown to customers when selecting the rate.'),
      '#default_value' => $this->configuration['rate_label'],
      '#required' => TRUE,
    ];
    $form['rates_group']['rate_description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rate description'),
      '#description' => $this->t('Provides additional details about the rate to the customer.'),
      '#default_value' => $this->configuration['rate_description'],
    ];
    $form['rates_group']['rate_amount'] = [
      '#type' => 'commerce_price',
      '#title' => $this->t('Rate amount'),
      '#default_value' => $amount,
      '#required' => TRUE,
    ];

    $form['api_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Loko API settings'),
    ];
    $form['api_settings']['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Loko API domain'),
      '#default_value' => $this->configuration['domain'],
      '#description' => $this->t('Domain should starts with <strong>https://</strong> and not contain <strong>/</strong> at the end of line, for example <strong>https://api.mint-dispatch.com</strong>.'),
      '#required' => TRUE,
      '#element_validate' => [
        [$this, 'validationDomain'],
      ],
    ];

    $form['api_settings']['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Loko API token'),
      '#required' => TRUE,
      '#description' => $this->t("This token can be found in your dashboard in 'COMPANY DETAILS' section on the Settings page."),
      '#default_value' => $this->configuration['token'],
      '#maxlength' => 824,
      '#pattern' => '\w{52}\.\w{727}\.\w{43}',
      '#element_validate' => [
        [$this, 'validationToken'],
      ],
    ];

    return $form;
  }

  /**
   * Validate Loko API domain.
   */
  public function validationDomain(&$element, FormStateInterface $form_state, &$complete_form): void {
    $domain = $element['#value'];

    // Check if domain is valid.
    if (!filter_var($domain, FILTER_VALIDATE_URL)) {
      $form_state->setError($element, $this->t('The domain is not valid.'));
    }

    // Check if domain is valid.
    if (str_ends_with($domain, '/')) {
      $form_state->setError($element, $this->t('The domain should not ends with "/".'));
    }
  }

  /**
   * Validate Loko API token.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function validationToken(&$element, FormStateInterface $form_state, &$complete_form): void {
    $this->setConfiguration([
      'domain' => $form_state->getValue(
        ['plugin', '0', 'target_plugin_configuration',
          'loko_shipment', 'api_settings', 'domain',
        ]
      ),
    ]);
    $this->setConfiguration(['token' => $element['#value']]);
    $this->lokoClient->setShipmentByPlugin($this);

    if (!$this->lokoClient->validateToken()) {
      $form_state->setError($element, new TranslatableMarkup('Token is invalid.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $values = $form_state->getValue($form['#parents']);

    $this->configuration['rate_label'] = $values['rates_group']['rate_label'];
    $this->configuration['rate_description'] = $values['rates_group']['rate_description'];
    $this->configuration['rate_amount'] = $values['rates_group']['rate_amount'];
    $this->configuration['domain'] = $values['api_settings']['domain'];
    $this->configuration['token'] = $values['api_settings']['token'];

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment): array {
    $rates = [];
    $rates[] = new ShippingRate([
      'shipping_method_id' => $this->parentEntity->id(),
      'service' => $this->services['default'],
      'amount' => Price::fromArray($this->configuration['rate_amount']),
      'description' => $this->configuration['rate_description'],
    ]);

    return $rates;
  }

}
