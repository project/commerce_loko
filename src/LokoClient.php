<?php

namespace Drupal\commerce_loko;

use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodInterface as PluginShippingMethodInterface;
use Drupal\commerce_shipping\Entity\ShippingMethodInterface as EntityShippingMethodInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Provides a service for commerce Loko Client.
 */
class LokoClient implements LokoClientInterface {

  /**
   * The shipping method storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected EntityStorageInterface $shippingMethodStorage;

  /**
   * The shipping method entity.
   *
   * @var \Drupal\commerce_shipping\Entity\ShippingMethodInterface
   */
  private EntityShippingMethodInterface $shipmentMethodEntity;

  /**
   * The shipping method plugin.
   *
   * @var \Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodInterface
   */
  private PluginShippingMethodInterface $shipmentMethodPlugin;

  /**
   * The configured API domain from the shipment method.
   *
   * @var string
   */
  private string $apiDomain;

  /**
   * The configured API token from the shipment method.
   *
   * @var string
   */
  private string $apiToken;

  /**
   * Constructor for the LokoClient Service.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    protected ClientInterface $httpClient,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    $this->shippingMethodStorage = $entityTypeManager->getStorage('commerce_shipping_method');
  }

  /**
   * {@inheritdoc}
   */
  public function setShipmentByPlugin(PluginShippingMethodInterface $shipping_method_plugin): void {
    if ($shipping_method_plugin->getPluginId() !== 'loko_shipment') {
      throw new \InvalidArgumentException('You need to set a LokoShipment shipping method.');
    }

    $this->shipmentMethodPlugin = $shipping_method_plugin;
    $this->apiToken = $shipping_method_plugin->getConfiguration()['token'];
    $this->apiDomain = $shipping_method_plugin->getConfiguration()['domain'];
  }

  /**
   * {@inheritdoc}
   */
  public function setShipmentByEntityId(string $id): void {

    /** @var \Drupal\commerce_shipping\Entity\ShippingMethodInterface $shipping_method_entity */
    $shipping_method_entity = $this->shippingMethodStorage->load($id);

    /** @var \Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodInterface $shipping_method_plugin */
    $shipping_method_plugin = $shipping_method_entity->getPlugin();

    if ($shipping_method_entity instanceof EntityShippingMethodInterface) {
      $this->shipmentMethodEntity = $shipping_method_entity;
      $this->setShipmentByPlugin($shipping_method_plugin);
    }
    else {
      throw new \InvalidArgumentException('The object must be an instance of \Drupal\commerce_shipping\Entity\ShippingMethodInterface.');
    }
  }

  /**
   * Base method for the API request.
   *
   * @param string $method
   *   HTTP method.
   * @param string $apiPath
   *   URL string.
   * @param array $options
   *   Request options to apply. See \GuzzleHttp\RequestOptions.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Representation of an outgoing, server-side response.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function apiBaseRequest(string $method, string $apiPath, array $options = []): ResponseInterface {
    if (empty($this->apiDomain) || empty($this->apiToken)) {
      throw new \InvalidArgumentException('You need to configure your Loko Shipment shipping method.');
    }

    $options = array_merge_recursive($options, [
      'headers' => [
        'Authorization' => 'Bearer ' . $this->apiToken,
        'accept' => 'text/plain',
      ],
      'http_errors' => FALSE,
    ]);

    return $this->httpClient->request($method, $this->apiDomain . $apiPath, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function validateToken(): bool {
    $response = $this->apiBaseRequest('GET', self::API_PATH_CURRENT_COMPANY);

    return $response->getStatusCode() === 200;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getAllParcelLockers(): array {
    $response = $this->apiBaseRequest('GET', self::API_PATH_ALL_PARCEL_LOCKERS);

    return Json::decode((string) $response->getBody());
  }

}
